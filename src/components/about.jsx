import React, { Component } from "react";
import "../components/about.css";

class About extends Component {
  render() {
    return (
      <div className="aboutWrapper row">
        <div className="col-md-6">
          <div className="flexWrap">
            <img src="./aboutVector.png" />
          </div>
        </div>
        <div className="col-md-6">
          <div className="flexWrap">
            <h2>Berteman dengan adaTeman</h2>
            <h4>Lorem ipsum dolor sit amet</h4>
            <p>
              Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum
              dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet.
              Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit
              amet. Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem
              ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit
              amet. Ipsum dolor sit amet. Lorem ipsum dolor sit amet. Ipsum
              dolor sit amet. Lorem ipsum dolor sit amet. Ipsum dolor sit amet.
            </p>
            <button className="buttonStyle buttonSize">ABOUT</button>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
