import React, { Component } from "react";
import Carousel from "nuka-carousel";
import "../components/hero.css";

class HeroSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="hero">
        <div className="heroOverlay" />
        <div className="textContainer">
          <h1 className="headerStyle">tenang aja,</h1>
          <h1 className="headerStyle">kan adateman</h1>
          <p className="headerSub">
            Lorem ipsum dolor sit amet. Ipsum dolor sit amet.
          </p>
          <button className="buttonStyle">DOWNLOAD</button>
        </div>
        <Carousel
          className="heroStyle"
          Autoplay={true}
          renderCenterLeftControls={false}
          renderCenterRightControls={false}
          renderBottomCenterControls={false}
        >
          <img className="imgStyle" src="./hero.png" />
          <img
            className="imgStyle"
            src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide2"
            alt=""
          />
          <img
            className="imgStyle"
            src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide3"
            alt=""
          />
          <img
            className="imgStyle"
            src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide4"
            alt=""
          />
          <img
            className="imgStyle"
            src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide5"
            alt=""
          />
          <img
            className="imgStyle"
            src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide6"
            alt=""
          />
        </Carousel>
      </div>
    );
  }
}

export default HeroSlider;
