import React, { Component } from "react";
import "../components/footer.css";

class Footer extends Component {
  render() {
    let arr = ["Facebook", "Instagram", "Line", "Twitter", "WhatsApp"];
    let logos = arr.map(image => {
      return (
        <img
          key={image}
          src={`./footer/socmed/${image}.svg`}
          alt=""
          className="socmed"
        />
      );
    });
    return (
      <div className="fullWidth">
        <div className="row footer">
          <div className="f-section col-md-4">
            <img src="./footer/logo.svg" />
          </div>
          <div className="f-section col-md-2">menu1</div>
          <div className="f-section col-md-2">menu2</div>
          <div className="f-section col-md-4">
            <div className="logoWrapper">{logos}</div>
            <br />
            <img className="asset" src="./footer/asset.svg" />
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
