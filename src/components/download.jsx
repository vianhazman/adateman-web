import React, { Component } from "react";
import MobileStoreButton from "react-mobile-store-button";

import "../components/download.css";

class Download extends Component {
  render() {
    return (
      <section className="downloadWrapper row">
        <div className="flexWrap">
          <h1>Download adaTeman </h1>
          <div>
            <MobileStoreButton store="ios" className="buttonWrapper" url="" />
            <MobileStoreButton
              store="android"
              className="buttonWrapper"
              url=""
            />
          </div>
        </div>
      </section>
    );
  }
}

export default Download;
