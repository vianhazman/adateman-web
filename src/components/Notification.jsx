import React, { Component } from "react";
import "../components/Notification.css";
import { Link } from "react-router-dom";

class Notification extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="NotificationContainer">
        <img src={this.props.image} />
        <h1>{this.props.h1}</h1>
        <p>{this.props.p}</p>
        <Link to="/">
          <button className="buttonStyle">HOME</button>
        </Link>
      </div>
    );
  }
}

export default Notification;
