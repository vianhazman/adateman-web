import React, { Component } from "react";
class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "rgba(255,255,255,0.9)"
    };
  }
  componentDidMount = () => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 500) {
        this.setState({
          color: "white"
        });
      } else {
        this.setState({
          color: "rgba(255,255,255,0.9)"
        });
      }
    });
  };

  render() {
    const bgNav = {
      background: this.state.color
    };
    return (
      <nav
        style={bgNav}
        className="navbar fixed-top navbar-expand-lg navbar-light"
      >
        <div className="container">
          <img className="navbar-brand" src="./logo_navbar.png" />
        </div>
        <div>
          <button className="buttonStyle">SIGN UP</button>
        </div>
      </nav>
    );
  }
}

export default Navbar;
