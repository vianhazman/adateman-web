import React, { Component } from "react";
import Carousel from "nuka-carousel";
import "../components/postSlider.css";

class PostSlider extends React.Component {
  render() {
    return (
      <Carousel
        className="heroStyle"
        cellAlign="left"
        slidesToShow={4}
        dragging={false}
        renderBottomCenterControls={false}
      >
        <div className="postItem">
          <img src="./posts/1.png" />
          <div class="overlay">
            <div class="text">Hello World</div>
          </div>
        </div>
        <div className="postItem">
          <img src="./posts/2.png" />
          <div class="overlay">
            <div class="text">Hello World</div>
          </div>
        </div>
        <div className="postItem">
          <img src="./posts/3.png" />
          <div class="overlay">
            <div class="text">Hello World</div>
          </div>
        </div>
        <div className="postItem">
          <img src="./posts/1.png" />
          <div class="overlay">
            <div class="text">Hello World</div>
          </div>
        </div>
        <div className="postItem">
          <img src="./posts/2.png" />
          <div class="overlay">
            <div class="text">Hello World</div>
          </div>
        </div>
        <div className="postItem">
          <img src="./posts/3.png" />
          <div class="overlay">
            <div class="text">Hello World</div>
          </div>
        </div>
      </Carousel>
    );
  }
}

export default PostSlider;
