import React, { Component } from "react";
import "../components/howTo.css";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

class HowTo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stepImage: "./howTo/1.png",
      classActive: 1
    };
  }

  handleClick = index =>
    this.setState({
      classActive: index,
      stepImage: "./howTo/" + index + ".png"
    });

  render() {
    return (
      <div className="howToWrapper">
        <h2>How To Use</h2>
        <p>Lorem Ipsum Dolor Amet</p>
        <div className="wrapper row">
          <div className="col-md-6">
            <ReactCSSTransitionGroup
              transitionName="background"
              transitionEnterTimeout={3000}
              transitionLeaveTimeout={3000}
            >
              <img className="screen" src={this.state.stepImage} />
            </ReactCSSTransitionGroup>
          </div>
          <div>
            <div
              className={
                this.state.classActive === 1 ? "mobActive" : "mobInactive"
              }
            >
              <h3>Cari barang yang Kamu butuhkan</h3>
              <p>
                Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum
                dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet.
                Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit
                amet.
              </p>
            </div>
            <div
              className={
                this.state.classActive === 2 ? "mobActive" : "mobInactive"
              }
            >
              <h3>Chat pemilik barangnya</h3>
              <p>
                Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum
                dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet.
                Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit
                amet.
              </p>
            </div>
            <div
              className={
                this.state.classActive === 3 ? "mobActive" : "mobInactive"
              }
            >
              <h3>Tunggu konfirmasi permintaan Kamu</h3>
              <p>
                Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum
                dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet.
                Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit
                amet.
              </p>
            </div>
            <div
              className={
                this.state.classActive === 4 ? "mobActive" : "mobInactive"
              }
            >
              <h3>Pinjam dan dapatkan barangnya!</h3>
              <p>
                Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum
                dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet.
                Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit
                amet.
              </p>
            </div>
          </div>
          <div className="stepsMobile col-md-6">
            <Steps
              index={1}
              currentIndex={this.state.classActive}
              isActive={this.state.classActive === 1}
              handleClick={this.handleClick}
              heading={"Cari barang yang Kamu butuhkan"}
              desc={
                "Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit amet."
              }
            />
            <Steps
              index={2}
              currentIndex={this.state.classActive}
              isActive={this.state.classActive === 2}
              handleClick={this.handleClick}
              heading={"Chat pemilik barangnya"}
              desc={
                "Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit amet."
              }
            />
            <Steps
              index={3}
              currentIndex={this.state.classActive}
              isActive={this.state.classActive === 3}
              handleClick={this.handleClick}
              heading={"Tunggu konfirmasi permintaan Kamu"}
              desc={
                "Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit amet."
              }
            />
            <Steps
              index={4}
              currentIndex={this.state.classActive}
              isActive={this.state.classActive === 3}
              handleClick={this.handleClick}
              heading={"Pinjam dan dapatkan barangnya!"}
              desc={
                "Lorem ipsum dolor sit amet. Ipsum dolor sit amet. Lorem ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit amet.Lorem ipsum dolor sit amet. Ipsum dolor sit amet."
              }
            />
          </div>
        </div>
      </div>
    );
  }
}
class StepsButton extends Component {
  handleClick = () => this.props.onClick(this.props.index);
  render() {
    return (
      <button
        type="button"
        className={
          this.props.currentIndex === this.props.index ? "fill" : "strokes"
        }
        onClick={this.handleClick}
      >
        <span>{this.props.index}</span>
      </button>
    );
  }
}

class Steps extends Component {
  render() {
    return (
      <div>
        <div className="mobile">
          <StepsButton
            index={this.props.index}
            onClick={this.props.handleClick}
            currentIndex={this.props.currentIndex}
          />
        </div>
        <div className="web">
          <div className="steps row">
            <div className="leftPane col-md-1">
              <StepsButton
                index={this.props.index}
                onClick={this.props.handleClick}
                currentIndex={this.props.currentIndex}
              />
              <img
                style={
                  this.props.index === 4
                    ? { display: "none" }
                    : { display: "block" }
                }
                src="./howTo/arrow.png"
              />
            </div>
            <div className="col-md">
              <div className="text">
                <h3
                  className={
                    this.props.currentIndex === this.props.index
                      ? "active"
                      : " "
                  }
                >
                  {this.props.heading}
                </h3>
                <p>{this.props.desc}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HowTo;
