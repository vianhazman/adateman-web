import React, { Component } from "react";
import "../components/guide.css";

class Guide extends Component {
  render() {
    return (
      <div className="guideWrapper row">
        <div className="col-md-6">
          <div className="flexWrap">
            <h1>
              What you have to know before using adaTeman for ur daily apps
            </h1>
            <button className="buttonStyle buttonSize">MORE DETAIL</button>
          </div>
        </div>
        <div className="col-md-6">
          <div className="flexWrap">
            <img src="./vector.svg" />
          </div>
        </div>
      </div>
    );
  }
}

export default Guide;
