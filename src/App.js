import React, { Component } from "react";
import "./App.css";
import Navbar from "./components/navbar";
import HeroSlider from "./components/hero";
import HowTo from "./components/howTo";
import Guide from "./components/guide";
import About from "./components/about";
import Download from "./components/download";
import PostSlider from "./components/postSlider";
import Footer from "./components/footer";
import Notification from "./components/Notification";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const Home = () => (
  <div>
    <HeroSlider />
    <HowTo />
    <Guide />
    <About />
    <Download />
  </div>
);

const NoMatch = () => (
  <Notification
    image="./404.svg"
    h1="Seems like you’re lost"
    p="But don’t worry we show you the way"
  />
);

const ComingSoon = () => (
  <Notification
    image="./comingsoon.svg"
    h1="Something awesome is coming soon"
    p="Kami sedang bekerja keras untuk membuat sesuatu yang luar biasa!"
  />
);

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/blog" component={ComingSoon} />
            <Route component={NoMatch} />
          </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
